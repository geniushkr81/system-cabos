import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { ContactStackNavigator } from "./StackNavigation";
import TabNavigator from "./TabNavigator";

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Aplicacion" component={TabNavigator} />
            <Drawer.Screen name="Configuracion" component={ContactStackNavigator}/>
        </Drawer.Navigator>
    );
}

export default DrawerNavigator;