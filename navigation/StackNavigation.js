import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "../screens/Home";
import About from "../screens/About";
import Contact from "../screens/Contact";

const Stack = createStackNavigator();

const MainStackNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} options={{headerShown:false}} />
            <Stack.Screen name="About" component={About} options={{headerShown:false}}/>
        </Stack.Navigator>
    );
}

const ContactStackNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Contact" component={Contact} options={{headerShown:false}}/>
        </Stack.Navigator>
    );
}

export { MainStackNavigator, ContactStackNavigator};